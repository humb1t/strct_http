use super::{Version, Path};
use request::Method;

pub type Headers = Vec<Header>;

/// HTTP messages, as defined in HTTP/1.1 and earlier, are human-readable. 
/// In HTTP/2, these messages are embedded into a binary structure, a frame,
/// allowing optimizations like compression of headers and multiplexing.
pub enum Message {
    Request{
        method: Method,
        path: Path,
        version: Version,
        headers: Headers,
    },
    Response,
}

/// A cacheable response is an HTTP response that can be cached, that is stored to be retrieved and
/// used later, saving a new request to the server.
/// [Definition](https://developer.mozilla.org/en-US/docs/Glossary/cacheable).
trait Cacheable {}

/// An HTTP method is idempotent if an identical request can be made once or several times in a row
/// with the same effect while leaving the server in the same state.
/// [Definition](https://developer.mozilla.org/en-US/docs/Glossary/idempotent).
/// [Specification](https://tools.ietf.org/html/rfc7231#section-4.2.2).
trait Idempotent {}

/// An HTTP method is safe if it doesn't alter the state of the server.
/// [Definition](https://developer.mozilla.org/en-US/docs/Glossary/safe).
/// [Specification](https://tools.ietf.org/html/rfc7231#section-4.2.1).
trait Safe : Idempotent {}

/// General headers apply to both requests and responses, but with no relation to the data transmitted in the body.
trait General {}

/// Request headers contain more information about the resource to be fetched, or about the client requesting the resource.
trait Request {}

/// Response headers hold additional information about the response, like its location or about the server providing it.
trait Response {}

/// Entity headers contain information about the body of the resource, like its content length or MIME type.
trait Entity {}

/// These headers must be transmitted to the final recipient of the message: the server for a request, or the client for a response. Intermediate proxies must retransmit these headers unmodified and caches must store them.
trait EndToEnd {}

/// These headers are meaningful only for a single transport-level connection, and must not be retransmitted by proxies or cached. Note that only hop-by-hop headers may be set using the Connection general header.
trait HopByHop {}

/// HTTP headers let the client and the server pass additional information with an HTTP request or response.
/// [Definition](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers).
pub enum Header {
}

pub mod request {
/*
    impl Safe for Method::Get {}
    impl Safe for Method::Head {}
    impl Safe for Method::Options {}
    impl Idempotent for Method::Put {}
    impl Idempotent for Method::Delete {}
*/
    ///HTTP defines a set of request methods to indicate the desired action to be performed for a given
    ///resource. Although they can also be nouns, these request methods are sometimes referred to as
    ///HTTP verbs. Each of them implements a different semantic, but some common features are shared
    ///by a group of them: e.g. a request method can be safe, idempotent, or cacheable.
    ///[Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content](https://tools.ietf.org/html/rfc7231#section-4).
    ///[PATCH Method for HTTP](https://tools.ietf.org/html/rfc5789#section-2).
    pub enum Method {
        ///The GET method requests a representation of the specified resource. Requests using GET should
        ///only retrieve data.
        Get,
        ///The HEAD method asks for a response identical to that of a GET request, but without the
        ///response body.
        Head,
        ///The POST method is used to submit an entity to the specified resource, often causing a change
        ///in state or side effects on the server.
        Post,
        ///The PUT method replaces all current representations of the target resource with the request 
        ///payload.
        Put,
        ///The DELETE method deletes the specified resource.
        Delete,
        ///The CONNECT method establishes a tunnel to the server identified by the target resource.
        Connect,
        ///The OPTIONS method is used to describe the communication options for the target resource.
        Options,
        ///The TRACE method performs a message loop-back test along the path to the target resource.
        Trace,
        ///The PATCH method is used to apply partial modifications to a resource.
        Patch,
    }
}

mod response {
    /// HTTP response status codes indicate whether a specific HTTP request has been successfully 
    /// completed. Responses are grouped in five classes:
    /// * Informational responses (100–199),
    /// * Successful responses (200–299),
    /// * Redirects (300–399),
    /// * Client errors (400–499),
    /// * and Server errors (500–599).
    /// [RFC 2616 Specification](https://tools.ietf.org/html/rfc2616#section-10).
    /// [RFC 7231 Specification](https://tools.ietf.org/html/rfc7231#section-6.5.1).
    enum Status {
        /// This interim response indicates that everything so far is OK and that the client should continue the request, or ignore the response if the request is already finished.
        Continue=100,
        /// This code is sent in response to an Upgrade request header from the client, and indicates the protocol the server is switching to.
        SwitchingProtocol=101,
        /// This code indicates that the server has received and is processing the request, but no response is available yet.
        Processing=102,
        /// This status code is primarily intended to be used with the Link header, letting the user agent start preloading resources while the server prepares a response.
        EarlyHints=103,
        /// The request has succeeded. The meaning of the success depends on the HTTP method:
        /// GET: The resource has been fetched and is transmitted in the message body.
        /// HEAD: The entity headers are in the message body.
        /// PUT or POST: The resource describing the result of the action is transmitted in the message body.
        /// TRACE: The message body contains the request message as received by the server.
        Ok=200,
        /// The request has succeeded and a new resource has been created as a result. This is typically the response sent after POST requests, or some PUT requests.
        Created=201,
        /// The request has been received but not yet acted upon. It is noncommittal, since there is no way in HTTP to later send an asynchronous response indicating the outcome of the request. It is intended for cases where another process or server handles the request, or for batch processing.
        Accepted=202,
        /// This response code means the returned meta-information is not exactly the same as is available from the origin server, but is collected from a local or a third-party copy. This is mostly used for mirrors or backups of another resource. Except for that specific case, the "200 OK" response is preferred to this status.
        NonAuthoritativeInformation=203,
        /// There is no content to send for this request, but the headers may be useful. The user-agent may update its cached headers for this resource with the new ones.
        NoContent=204,
        /// Tells the user-agent to reset the document which sent this request.
        ResetContent=205,
        /// This response code is used when the Range header is sent from the client to request only part of a resource.
        PartialContent=206,
        /// Conveys information about multiple resources, for situations where multiple status codes might be appropriate.
        MultiStatus=207,
        /// Used inside a `<dav:propstat>` response element to avoid repeatedly enumerating the internal members of multiple bindings to the same collection.
        AlreadyReported=208,
        /// The server has fulfilled a GET request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance.
        ImUsed=226,
        /// The request has more than one possible response. The user-agent or user should choose one of them. (There is no standardized way of choosing one of the responses, but HTML links to the possibilities are recommended so the user can pick.)
        MultipleChoice=300,
        /// The URL of the requested resource has been changed permanently. The new URL is given in the response.
        MovedPermanently=301,
        /// This response code means that the URI of requested resource has been changed temporarily. Further changes in the URI might be made in the future. Therefore, this same URI should be used by the client in future requests.
        Found=302,
        /// The server sent this response to direct the client to get the requested resource at another URI with a GET request.
        SeeOther=303,
        /// This is used for caching purposes. It tells the client that the response has not been modified, so the client can continue to use the same cached version of the response.
        NotModified=304,
        /// Defined in a previous version of the HTTP specification to indicate that a requested response must be accessed by a proxy. It has been deprecated due to security concerns regarding in-band configuration of a proxy.
        UseProxy=305,
        /// This response code is no longer used; it is just reserved. It was used in a previous version of the HTTP/1.1 specification.
        Unused=306,
        /// The server sends this response to direct the client to get the requested resource at another URI with same method that was used in the prior request. This has the same semantics as the `302 Found` HTTP response code, with the exception that the user agent must not change the HTTP method used: If a `POST` was used in the first request, a `POST` must be used in the second request.
        TemporaryRedirect=307,
        /// This means that the resource is now permanently located at another URI, specified by the `Location:` HTTP Response header. This has the same semantics as the `301 Moved Permanently` HTTP response code, with the exception that the user agent must not change the HTTP method used: If a `POST` was used in the first request, a `POST` must be used in the second request.
        PermanentRedirect=308,
        /// The server could not understand the request due to invalid syntax.
        BadRequest=400,
        /// Although the HTTP standard specifies "unauthorized", semantically this response means "unauthenticated". That is, the client must authenticate itself to get the requested response.
        Unauthorized=401,
        /// This response code is reserved for future use. The initial aim for creating this code was using it for digital payment systems, however this status code is used very rarely and no standard convention exists.
        PaymentRequired=402,
        /// The client does not have access rights to the content; that is, it is unauthorized, so the server is refusing to give the requested resource. Unlike 401, the client's identity is known to the server.
        Forbidden=403,
        /// The server can not find the requested resource. In the browser, this means the URL is not recognized. In an API, this can also mean that the endpoint is valid but the resource itself does not exist. Servers may also send this response instead of 403 to hide the existence of a resource from an unauthorized client. This response code is probably the most famous one due to its frequent occurrence on the web.
        NotFound=404,
        /// The request method is known by the server but has been disabled and cannot be used. For example, an API may forbid DELETE-ing a resource. The two mandatory methods, GET and HEAD, must never be disabled and should not return this error code.
        MethodNotAllowed=405,
        /// This response is sent when the web server, after performing server-driven content negotiation, doesn't find any content that conforms to the criteria given by the user agent.
        NotAcceptable=406,
        /// This is similar to 401 but authentication is needed to be done by a proxy.
        ProxyAuthenticationRequired=407,
        /// This response is sent on an idle connection by some servers, even without any previous request by the client. It means that the server would like to shut down this unused connection. This response is used much more since some browsers, like Chrome, Firefox 27+, or IE9, use HTTP pre-connection mechanisms to speed up surfing. Also note that some servers merely shut down the connection without sending this message.
        RequestTimeout=408,
        /// This response is sent when a request conflicts with the current state of the server.
        Conflict=409,
        /// This response is sent when the requested content has been permanently deleted from server, with no forwarding address. Clients are expected to remove their caches and links to the resource. The HTTP specification intends this status code to be used for "limited-time, promotional services". APIs should not feel compelled to indicate resources that have been deleted with this status code.
        Gone=410,
        /// Server rejected the request because the `Content-Length` header field is not defined and the server requires it.
        LengthRequired=411,
        /// The client has indicated preconditions in its headers which the server does not meet.
        PreconditionFailed=412,
        /// Request entity is larger than limits defined by server; the server might close the connection or return an `Retry-After` header field.
        PayloadTooLarge=413,
        /// The URI requested by the client is longer than the server is willing to interpret.
        UriTooLong=414,
        /// The media format of the requested data is not supported by the server, so the server is rejecting the request.
        UnsupportedMediaType=415,
        /// The range specified by the Range header field in the request can't be fulfilled; it's possible that the `range` is outside the size of the target URI's data.
        RangeNotSatisfiable=416,
        /// This response code means the expectation indicated by the `Expect` request header field can't be met by the server.
        ExpectationFailed=417,
        /// The server refuses the attempt to brew coffee with a teapot.
        ImaTeapot=418,
        /// The request was directed at a server that is not able to produce a response. This can be sent by a server that is not configured to produce responses for the combination of scheme and authority that are included in the request URI.
        MisdirectedRequest=421,
        /// The request was well-formed but was unable to be followed due to semantic errors.
        UnprocessableEntity=422,
        /// The resource that is being accessed is locked.
        Locked=423,
        /// The request failed due to failure of a previous request.
        FailedDependency=424,
        /// Indicates that the server is unwilling to risk processing a request that might be replayed.
        TooEarly=425,
        /// The server refuses to perform the request using the current protocol but might be willing to do so after the client upgrades to a different protocol. The server sends an `Upgrade` header in a 426 response to indicate the required protocol(s).
        UpgradeRequired=426,
        /// The origin server requires the request to be conditional. This response is intended to prevent the 'lost update' problem, where a client GETs a resource's state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.
        PreconditionRequired=428,
        /// The user has sent too many requests in a given amount of time ("rate limiting").
        TooManyRequests=429,
        /// The server is unwilling to process the request because its header fields are too large. The request may be resubmitted after reducing the size of the request header fields.
        RequestHeaderFieldsTooLarge=431,
        /// The user-agent requested a resource that cannot legally be provided, such as a web page censored by a government.
        UnavailableForLegalReasons=451,
        /// The server has encountered a situation it doesn't know how to handle.
        InternalServerError=500,
        /// The request method is not supported by the server and cannot be handled. The only methods that servers are required to support (and therefore that must not return this code) are `GET` and `HEAD`.
        NotImplemented=501,
        /// This error response means that the server, while working as a gateway to get a response needed to handle the request, got an invalid response.
        BadGateway=502,
        /// The server is not ready to handle the request. Common causes are a server that is down for maintenance or that is overloaded. Note that together with this response, a user-friendly page explaining the problem should be sent. This responses should be used for temporary conditions and the `Retry-After:` HTTP header should, if possible, contain the estimated time before the recovery of the service. The webmaster must also take care about the caching-related headers that are sent along with this response, as these temporary condition responses should usually not be cached.
        ServiceUnavailable=503,
        /// This error response is given when the server is acting as a gateway and cannot get a response in time.
        GatewayTimeout=504,
        /// The HTTP version used in the request is not supported by the server.
        HttpVersionNotSupported=505,
        /// The server has an internal configuration error: the chosen variant resource is configured to engage in transparent content negotiation itself, and is therefore not a proper end point in the negotiation process.
        VariantAlsoNegotiates=506,
        /// The method could not be performed on the resource because the server is unable to store the representation needed to successfully complete the request.
        InsufficientStorage=507,
        /// The server detected an infinite loop while processing the request.
        LoopDetected=508,
        /// Further extensions to the request are required for the server to fulfil it.
        NotExtended=510,
        /// The 511 status code indicates that the client needs to authenticate to gain network access.
        NetworkAuthenticationRequired=511,
    }
}
