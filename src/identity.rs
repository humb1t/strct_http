//! `Identity` module contains structures related to Resources Identifiers like `URI::URL` and
//! `URI::URN`.

use super::{MimeType, Parameters};

type Base64 = &'static str;
type Data = &'static [u64];
type Port = u8;
type Path = &'static str;
type Query = Option<Parameters>;

/// An IP address is a number assigned to every device connected to a network that uses the
/// Internet protocol.
#[derive(Clone, Copy, Debug)]
pub enum IpAddress {
    /// Internet Protocol version 4 (IPv4) defines an IP address as a 32-bit number.
    Ipv4(u32), 
    /// Internet Protocol version 6 (IPv6) defines and IP address as a 128-bit number.
    Ipv6(u128),
}

/// Common schemes and protocols.
//TODO: make it Copy
#[derive(Clone, Debug)]
pub enum Scheme {
    /// Data URLs are treated as unique opaque origins.
    /// [Definition](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs).
    Data(MimeType, Option<Base64>, Data),
    /// Host-specific file names.
    File,
    /// File Transfer Protocol.
    /// [Definition](https://developer.mozilla.org/en-US/docs/Glossary/FTP).
    Ftp,
    /// The HyperText Transfer Protocol is the underlying network protocol that enables
    /// transfer of hypermedia documents on the Web.
    Http,
    /// `HTTP` over a secure TLS channel.
    Https,
    /// URL-embedded JavaScript code.
    Javascript,
    /// Electronic mail address.
    Mailto,
    /// Secure shell.
    Ssh,
    /// Telephone.
    Tel,
    /// Uniform Resource Names.
    Urn,
    /// Source code of the resource.
    ViewSource,
    /// WebSocket connection.
    Ws,
    /// Encrypted WebSocket connection.
    Wss,
}

/// Authority that governs the namespace. It indicates which Web server is being requested.
#[derive(Clone, Copy, Debug)]
pub enum Authority {
    /// Domain Name as Web server `Authority`.
    DomainName(&'static str),
    /// IP Addreess as Web server `Authority`.
    IpAddress(IpAddress),
}

/// Uniform Resource Identifier is a string that refers to a resource.
/// [Definition](https://developer.mozilla.org/en-US/docs/Glossary/URI).
#[derive(Clone, Debug)]
pub enum URI {

    /// Uniform Resource Locator is a text string that specifies where a resource can be found on the Internet.
    /// [Definition](https://developer.mozilla.org/en-US/docs/Glossary/URL).
    URL(Scheme, Authority, Port, Path, Query),
    /// A Uniform Resource Name is a URI that identifies a resource by name in a particular namespace.
    URN(Scheme, Authority, Port, Path, Query),
}
